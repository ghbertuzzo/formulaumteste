/*
    Autor: Giovani Bertuzzo
    github.com/ghbertuzzo
 */
package RaceF1;

import java.util.ArrayList;

public class Driver {

    private String idDriver;
    private String nameDriver;
    private ArrayList<Lap> laps;
    private int positionRace;

    public Driver(String idDriver, String nameDriver) {
        this.idDriver = idDriver;
        this.nameDriver = nameDriver;
        this.laps = new ArrayList<>();
        this.positionRace = -1;
    }

    public void addLap(Lap lap) {
        this.laps.add(lap);
    }

    public long endTimeRace() {
        long endTime = 0;
        for (Lap lap : laps) {
            if (lap.getNumberLap() == 4) {
                endTime = lap.getTimeInit() + lap.getTimeLap();
            }
        }
        return endTime;
    }

    public Lap bestLap() {
        Lap betterLap = laps.get(0);
        for (Lap lap : laps) {
            if (lap.getTimeLap() < betterLap.getTimeLap()) {
                betterLap = lap;
            }
        }
        return betterLap;
    }

    public Double speedAvg() {
        Double sum = 0.0;
        int cont = 0;
        for (Lap lap : laps) {
            sum += lap.getAvgSpeed();
            cont++;
        }
        return sum / cont;
    }

    public long totalTime() {
        long sum = 0;
        for (Lap lap : laps) {
            sum += lap.getTimeLap();
        }
        return sum;
    }

    public String getIdDriver() {
        return idDriver;
    }

    public void setIdDriver(String idDriver) {
        this.idDriver = idDriver;
    }

    public String getNameDriver() {
        return nameDriver;
    }

    public void setNameDriver(String nameDriver) {
        this.nameDriver = nameDriver;
    }

    public ArrayList<Lap> getLaps() {
        return laps;
    }

    public void setLaps(ArrayList<Lap> laps) {
        this.laps = laps;
    }

    public int getPositionRace() {
        return positionRace;
    }

    public void setPositionRace(int positionRace) {
        this.positionRace = positionRace;
    }

}
