/*
    Autor: Giovani Bertuzzo
    github.com/ghbertuzzo
 */
package RaceF1;

public class Lap {

    private long timeInit;
    private String idDriver;
    private String nameDriver;
    private int numberLap;
    private long timeLap;
    private Double avgSpeed;

    public Lap(long timeInit, String idDriver, String nameDriver, int numberLap, long timeLap, Double avgSpeed) {
        this.timeInit = timeInit;
        this.idDriver = idDriver;
        this.nameDriver = nameDriver;
        this.numberLap = numberLap;
        this.timeLap = timeLap;
        this.avgSpeed = avgSpeed;
    }

    public long getTimeInit() {
        return timeInit;
    }

    public String getIdDriver() {
        return idDriver;
    }

    public String getNameDriver() {
        return nameDriver;
    }

    public int getNumberLap() {
        return numberLap;
    }

    public long getTimeLap() {
        return timeLap;
    }

    public Double getAvgSpeed() {
        return avgSpeed;
    }

}
