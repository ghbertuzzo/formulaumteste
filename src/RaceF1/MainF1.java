/*
    Autor: Giovani Bertuzzo
    github.com/ghbertuzzo
 */
package RaceF1;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;

public class MainF1 {

    public static void main(String[] args) throws IOException, ParseException {
        // TODO code application logic here
        File arquivo = new File("arquivo_de_entrada.txt");
        Race corrida = new Race(arquivo);
        corrida.start();
    }

}
