/*
    Autor: Giovani Bertuzzo
    github.com/ghbertuzzo
 */
package RaceF1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Locale;

public class Race {

    private ArrayList<String> list;
    private File file;
    private ArrayList<Lap> laps;
    private ArrayList<Driver> drivers;
    private Utils utils;

    public Race(File arquivo) {
        this.list = new ArrayList<>();
        this.file = arquivo;
        this.laps = new ArrayList<>();
        this.drivers = new ArrayList<>();
        this.utils = new Utils();
    }

    public void start() throws IOException, ParseException {
        loadFile();
        interpretFile();
        generateDrivers();
        setPositions();
        showMenu();
    }

    private void loadFile() throws FileNotFoundException, IOException {
        System.out.print("Reading file...");
        FileReader fr = new FileReader(this.getFile());
        BufferedReader br = new BufferedReader(fr);
        br.readLine(); //IGNORA CABECALHO
        while (br.ready()) {
            this.getList().add(br.readLine());
        }
        br.close();
        fr.close();
        System.out.println(" Completed.");
    }

    private void interpretFile() throws ParseException {
        for (String line : this.getList()) {
            String[] splited = line.split("\\s+");
            long timeInit = getUtils().convertStringToLong(splited[0]);
            String idDriver = splited[1];
            String nameDriver = splited[3];
            int numberLap = Integer.parseInt(splited[4]);
            long timeLap = getUtils().convertStringToLong(splited[5]);
            NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
            Number number = format.parse(splited[6]);
            Double avgSpeed = number.doubleValue();
            Lap lap = new Lap(timeInit, idDriver, nameDriver, numberLap, timeLap, avgSpeed);
            this.getLaps().add(lap);
        }
    }

    private void setPositions() {
        ArrayList<Driver> copy = new ArrayList<Driver>(this.getDrivers());
        int[] listRemove = new int[copy.size()];
        int cont = 0;
        for (int i = 0; i < copy.size(); i++) {
            if (copy.get(i).endTimeRace() == 0) {
                listRemove[cont] = i;
                cont++;
            }
        }
        ArrayList<Driver> removed = new ArrayList<>();
        for (int j = 0; j < cont; j++) {
            removed.add(copy.remove(listRemove[j]));
        }
        copy.sort(Comparator.comparing(Driver::endTimeRace));
        for (Driver driver : removed) {
            copy.add(driver);
        }
        for (int i = 0; i < copy.size(); i++) {
            for (Driver driver : this.getDrivers()) {
                if (copy.get(i).getIdDriver().equals(driver.getIdDriver())) {
                    driver.setPositionRace(i + 1);
                }
            }
        }
    }

    private void generateTxt() throws FileNotFoundException, UnsupportedEncodingException {
        this.getDrivers().sort(Comparator.comparing(Driver::getPositionRace));
        PrintWriter writer = new PrintWriter("arquivo_de_saida.txt", "UTF-8");
        writer.println("Posição Chegada;Código Piloto;Nome Piloto; Qtde Voltas Completadas;Tempo Total de Prova");
        for (Driver driver : this.getDrivers()) {
            writer.println(driver.getPositionRace() + ";" + driver.getIdDriver() + ";" + driver.getNameDriver() + ";" + driver.getLaps().size() + ";" + getUtils().convertLongToString(driver.totalTime()));
        }
        writer.close();
        System.out.println("Generated file 'arquivo_de_saida.txt'");
    }

    private void showTimeAfterWinner() {
        Driver winner = null;
        for (Driver driver : this.getDrivers()) {
            if (driver.getPositionRace() == 1) {
                winner = driver;
            }
        }
        System.out.println("\nTIME AFTER WINNER\n");
        for (Driver driver : this.getDrivers()) {
            long extraTime = driver.endTimeRace() - winner.endTimeRace();
            System.out.println(getUtils().convertLongToString(extraTime) + " - " + driver.getNameDriver());
        }
    }

    private void showAvgSpeedDrivers() {
        System.out.println("\nAVERAGE SPEED(DRIVERS)");
        for (Driver driver : this.getDrivers()) {
            System.out.println("\nDriver: " + driver.getNameDriver());
            System.out.println("Average Speed: " + String.format("%.2f", driver.speedAvg()));
        }
    }

    private void showBestLapDrivers() {
        System.out.println("\nBEST LAP IN RACE(DRIVERS)");
        for (Driver driver : this.getDrivers()) {
            Lap best = driver.bestLap();
            System.out.println("\nDriver: " + best.getNameDriver());
            System.out.println("Lap: " + best.getNumberLap());
            System.out.println("Time: " + getUtils().convertLongToString(best.getTimeLap()));
        }
    }

    private void showBestLapInRace() {
        Lap lap = bestLapInRace();
        System.out.println("\nBEST LAP IN RACE\n");
        System.out.println("Driver: " + lap.getNameDriver());
        System.out.println("Lap: " + lap.getNumberLap());
        System.out.println("Time: " + getUtils().convertLongToString(lap.getTimeLap()));
    }

    private Lap bestLapInRace() {
        Lap best = null;
        long bestTime = Long.MAX_VALUE;
        for (Lap lap : this.getLaps()) {
            if (lap.getTimeLap() < bestTime) {
                bestTime = lap.getTimeLap();
                best = lap;
            }
        }
        return best;
    }

    private void generateDrivers() {
        for (Lap lap : this.getLaps()) {
            if (!driverExist(lap.getIdDriver())) {
                createDriver(lap.getIdDriver(), lap.getNameDriver());
            }
            Driver driver = getDriver(lap.getIdDriver());
            driver.addLap(lap);
        }
    }

    private Driver getDriver(String idDriver) {
        for (Driver driver : this.getDrivers()) {
            if (idDriver.equals(driver.getIdDriver())) {
                return driver;
            }
        }
        return null;
    }

    private boolean driverExist(String idDriver) {
        for (Driver driver : this.getDrivers()) {
            if (idDriver.equals(driver.getIdDriver())) {
                return true;
            }
        }
        return false;
    }

    private void createDriver(String idDriver, String nameDriver) {
        Driver driver = new Driver(idDriver, nameDriver);
        this.getDrivers().add(driver);
    }

    private void showMenu() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int resp = -1;
        while (resp != 0) {
            System.out.println("\n-- MENU --");
            System.out.println("\n1 - Generate txt output file.");
            System.out.println("2 - Show best lap of the race.");
            System.out.println("3 - Show best lap of each drivers.");
            System.out.println("4 - Show average speed per driver.");
            System.out.println("5 - Show arrival time after winner per driver.");
            System.out.print("0 - Exit.\n\n> ");
            resp = Integer.parseInt(reader.readLine());
            process(resp);
        }
    }

    private void process(int resp) throws FileNotFoundException, UnsupportedEncodingException {
        switch (resp) {
            case 0:
                break;
            case 1:
                generateTxt();
                break;
            case 2:
                showBestLapInRace();
                break;
            case 3:
                showBestLapDrivers();
                break;
            case 4:
                showAvgSpeedDrivers();
                break;
            case 5:
                showTimeAfterWinner();
                break;
            default:
                System.out.println("Invalid value!");
                break;
        }
    }    

    public ArrayList<String> getList() {
        return list;
    }

    public File getFile() {
        return file;
    }

    public ArrayList<Lap> getLaps() {
        return laps;
    }

    public ArrayList<Driver> getDrivers() {
        return drivers;
    }
    
    public Utils getUtils() {
        return utils;
    }

}
