/*
    Autor: Giovani Bertuzzo
    github.com/ghbertuzzo
 */
package RaceF1;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class Utils {
    
    public long convertStringToLong(String time) throws ParseException {
        time = verify(time);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = sdf.parse("1970-01-01 " + time);
        return date.getTime();
    }

    public String convertLongToString(long value) {
        Date dataa = new Date(value);
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss.SSS");
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        return verify(formatter.format(dataa));
    }

    public String verify(String oth) {
        String complet = null;
        switch (oth.toCharArray().length) {
            case 1:
                complet = "00:00:00.00" + oth;
                break;
            case 2:
                complet = "00:00:00.0" + oth;
                break;
            case 3:
                complet = "00:00:00." + oth;
                break;
            case 4:
                complet = "00:00:00" + oth;
                break;
            case 5:
                complet = "00:00:0" + oth;
                break;
            case 6:
                complet = "00:00:" + oth;
                break;
            case 7:
                complet = "00:00" + oth;
                break;
            case 8:
                complet = "00:0" + oth;
                break;
            case 9:
                complet = "00:" + oth;
                break;
            case 10:
                complet = "00" + oth;
                break;
            case 11:
                complet = "0" + oth;
                break;
            case 12:
                complet = oth;
                break;
            default:
                System.out.println("Format invalid");
        }
        return complet;
    }
    
}
